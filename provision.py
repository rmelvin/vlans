import argparse
import csv
import os
from copy import deepcopy
from operator import itemgetter
from typing import List, Optional

DID = 'device_id'
PRIMARY_PORT = 'primary_port'
VID = 'vlan_id'
VLANS_COLS = (DID, PRIMARY_PORT, VID)
SECONDARY_PORT = 'secondary_port'

RID = 'request_id'
REDUNDANT = 'redundant'
REQUESTS_COLS = (RID, REDUNDANT)

OUTPUT_COLS = ('request_id', 'device_id', 'primary_port', 'vlan_id')

_cache = dict()


class IncompatibleRequestsFile(Exception):
    pass


class IncompatibleVlansFile(Exception):
    pass


def first(it, condition=lambda x: True):
    """Utility function to return first item in iterable matching condition."""
    return next((x for x in it if condition(x)), None)


def _is_truthy(value) -> bool:
    """Detect if the value is truthy."""
    return value in (True, 'True', 'true', 1, '1')


def _validate_vlans_file(file):
    """Validate the vlans csv has expected headers."""
    with open(file) as fptr:
        reader = csv.DictReader(fptr)
        for row in reader:
            if not all([col in row for col in VLANS_COLS]):
                raise IncompatibleVlansFile


def sort_vlans(data):
    """Sort the vlans data ahead of time as an optimization for the provisioner."""
    for device_id in data.keys():
        for port_type in (PRIMARY_PORT, SECONDARY_PORT):
            if port_type not in data[device_id]:
                continue
            data[device_id][port_type] = sorted(data[device_id][port_type])  # set -> list


def read_vlans(file) -> dict:
    """
    Read the vlans csv file and build the following data structure
    {
        <device0_id>: {
            'primary_port': [vlanid1, vlanid2, etc. ],
        },
        <device1_id>: {
            'primary_port': [vlanid1, vlanid2, etc. ],
            'secondary_port': [vlanid0, vlanid1, etc. ],
        }
        .
        .
    }
    where in the above example,
      - device0 only has a primary port
      - device1 has both primary and secondary ports
      - the vlan ids are arbitrary
    """
    _validate_vlans_file(file)
    data = dict()
    with open(file) as fptr:
        reader = csv.DictReader(fptr)
        for row in reader:
            did = int(row.get(DID))
            pp = _is_truthy(row.get(PRIMARY_PORT))
            vid = int(row.get(VID))
            if did not in data:
                data[did] = dict()
            if pp:
                port = PRIMARY_PORT
                if PRIMARY_PORT not in data[did]:
                    data[did][PRIMARY_PORT] = set()
            elif SECONDARY_PORT not in data[did]:
                port = SECONDARY_PORT
                data[did][SECONDARY_PORT] = set()
            data[did][port].add(vid)

    # Sort the vlan ids as an optimization.
    sort_vlans(data)
    return data


def _validate_requests_file(file):
    """Validate the requests csv has expected headers."""
    with open(file) as fptr:
        reader = csv.DictReader(fptr)
        for row in reader:
            if not all([col in row for col in REQUESTS_COLS]):
                raise IncompatibleRequestsFile


def read_requests(file) -> List[dict]:
    """
    Read the requests csv file and build the following data structure
    [
        {'request_id': <request0_id>, 'redundant': <0|1>},
        .
        .
        {'request_id': <requestN_id>, 'redundant': <0|1>},
    ]
    """
    _validate_requests_file(file)
    data = list()
    with open(file) as fptr:
        reader = csv.DictReader(fptr)
        for row in reader:
            data.append(row)
    return data


class VlanProvisioner:
    """Handle vlan id provisioning requests."""

    def __init__(self, vlans):
        self._cache = deepcopy(vlans)

    def _find_min_vlan_device_pairs(self, port_type=PRIMARY_PORT) -> List[tuple]:
        """
        Finds the min vlan ids for all devices for a specific port type.
        note: Assumes the vlan ids for a device's port type are already pre-sorted.

        :return list of (vlan_id, device_id) tuples sorted by vlan_id
        """
        vlan_device_pairs = [
            (self._cache[dev_id][port_type][0], dev_id)
            for dev_id in self._cache
        ]
        return sorted(vlan_device_pairs, key=itemgetter(0))

    def _filter_by_port_match(self, sorted_pairs: List[tuple]) -> List[tuple]:
        """
        Filter the provided sorted (vlan_id, device_id) minimum pairs by if there is a matching
        secondary port. This is used in redundant provisioning.

        :param sorted_pairs: list of (vlan_id, device_id) pairs sorted by vlan_id
        :return sorted pairs that also have a matching vlan_id as a secondary port.
        """
        return [
            (vlan, device)
            for vlan, device in sorted_pairs
            if (SECONDARY_PORT in self._cache[device] and
                vlan in self._cache[device][SECONDARY_PORT])
        ]

    def _find_min_vlan(self, sorted_pairs: List[tuple]) -> tuple:
        """
        Find minimum vlan id value from the provided sorted (vlan_id, device_id) pairs and detect
        if the value is duplicated.

        :param sorted_pairs: list of (vlan_id, device_id) pairs
        :return (vlan_id, dup)
        """
        if len(sorted_pairs) == 0:
            return None, False
        min_vlans, _ = list(zip(*sorted_pairs))
        vlan_id = min_vlans[0]
        dup = min_vlans.count(vlan_id) > 1
        return vlan_id, dup

    def _pick_device_from_tie(self, sorted_pairs: List[tuple], vlan_id: int) -> Optional[int]:
        """
        Picks a device if there are multiple devices that have the same minimum vlan id.
        Currently, chooses device with lowest id.

        :param sorted_pairs: list of (vlan_id, device_id) pairs
        """
        return first((did for vid, did in sorted_pairs if vlan_id == vid))

    def _release_vlan(self, device_id: int, port_type: str, vlan_id: int):
        """
        Release a vlan now that it has been acquired.
        """
        if not (device_id in self._cache and port_type in self._cache[device_id]):
            return
        if vlan_id not in self._cache[device_id][port_type]:
            return
        self._cache[device_id][port_type].remove(vlan_id)

    def _pick_device(self, sorted_pairs: List[tuple], vlan_id: Optional[int],
                     dup: bool=False) -> Optional[int]:
        """
        Pick a minimum device id from sorted (vlan_id, device_id) pairs.
        If no duplicate, then just pick device id from second element of first tuple since the list
        is already sorted. Else, handle duplicate (i.e. tie).
        """
        device_id = None
        if not dup:
            if (len(sorted_pairs) > 0 and
                    len(sorted_pairs[0]) > 1):
                device_id = sorted_pairs[0][1]
        else:
            device_id = self._pick_device_from_tie(sorted_pairs, vlan_id)
        return device_id

    def _expand_results(self, result: dict, redundant: bool=False) -> List[dict]:
        """
        Expand result into [secondary_result, primary_result] if it's a redundant request, else
        pass through the non-redundant request result as a list.
        """
        if not redundant:
            return [result]  # pass-through
        else:
            primary_port_result = result.copy()
            primary_port_result[PRIMARY_PORT] = 1  # primary port result
            result[PRIMARY_PORT] = 0  # secondary port result
            return [result, primary_port_result]  # maintain secondary, primary order

    def _process_request(self, redundant: bool=False) -> List[dict]:
        """
        Core processing of a single vlan provisioning request. We handle two types of requests:
        1) Requests that do not require redundancy - for these we would like to reserve a single
           VLAN ID that meets the following criteria:
            a) The VLAN ID should be the lowest available VLAN ID on any ​primary​ port.
            b) In the event of a tie, we would choose the VLAN ID on the device with the lowest
               device ID
        2) Requests that require redundancy - for these we would like to reserve a pair of VLAN IDs
           that meet the following criteria:
            a) One VLAN ID must be from a ​primary​ port and the other from a secondary​ port
            b) The two ports must be on the same device
            c) The VLAN IDs must be the same on both ports
            d) The VLAN IDs should be the lowest possible IDs that meet the above criteria
            e) Again, in the event of a tie, we would choose the VLAN IDs on the device with the
               lowest device ID
        """
        # TODO: DRYify logic
        if not redundant:
            primary_port = 1
            pairs_sorted_by_vlan = self._find_min_vlan_device_pairs()
            vlan_id, dup = self._find_min_vlan(pairs_sorted_by_vlan)
            result = {}
            if vlan_id is not None:
                device_id = self._pick_device(pairs_sorted_by_vlan, vlan_id, dup)
                if device_id is not None:
                    result = {
                        'device_id': device_id,
                        'primary_port': primary_port,
                        'vlan_id': vlan_id,
                    }
        else:
            pairs_sorted_by_vlan = self._find_min_vlan_device_pairs()
            primary_secondary_vlans_and_devices = self._filter_by_port_match(pairs_sorted_by_vlan)
            vlan_id, dup = self._find_min_vlan(primary_secondary_vlans_and_devices)
            result = {}
            if vlan_id is not None:
                device_id = self._pick_device(primary_secondary_vlans_and_devices, vlan_id, dup)
                if device_id is not None:
                    result = {
                        'device_id': device_id,
                        'vlan_id': vlan_id,
                    }
        return self._expand_results(result, redundant=redundant)

    def process_request(self, request: dict) -> List[dict]:
        """Process a single vlan provisioning request. Also handles releasing acquired vlans."""
        results = self._process_request(redundant=_is_truthy(request.get(REDUNDANT)))

        # Release acquired vlans
        for result in results:
            port_type = PRIMARY_PORT if _is_truthy(result.get(PRIMARY_PORT)) else SECONDARY_PORT
            self._release_vlan(result.get(DID), port_type, result.get(VID))

        return results

    def process_requests(self, requests: List[dict]) -> List[dict]:
        """Process all vlan provisioning requests. Take care of assembling results."""
        results = list()
        for request in requests:
            results_ = self.process_request(request)
            # Splice request id into result
            for result in results_:
                result[RID] = request.get(RID)
            results.extend(results_)
        return results

    def output_results(self, file, results: List[dict]):
        """Generate output csv with results of all requests."""
        with open(file, 'w') as fptr:
            writer = csv.DictWriter(fptr, OUTPUT_COLS)
            writer.writeheader()
            writer.writerows(results)


if __name__ == '__main__':
    # Parse command-line args
    parser = argparse.ArgumentParser(description='provisioner for vlans requests')
    parser.add_argument('vlans', type=str, help='name of the vlans csv')
    parser.add_argument('requests', type=str, help='name of the requests csv')
    parser.add_argument('output', type=str, help='name of the output csv')
    args = parser.parse_args()
    vlans_file = os.path.join(os.path.dirname(__file__), args.vlans)
    requests_file = os.path.join(os.path.dirname(__file__), args.requests)
    output_file = os.path.join(os.path.dirname(__file__), args.output)

    # Read input files
    vlans_data = read_vlans(vlans_file)
    requests = read_requests(requests_file)

    # Instantiate our vlan provisioner, process requests, and output results to csv.
    provisioner = VlanProvisioner(vlans_data)
    results = provisioner.process_requests(requests)
    provisioner.output_results(output_file, results)

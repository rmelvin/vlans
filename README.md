# Vlan Provisioner

## Problem
Given a network of devices, each device has a primary port. Some devices also have a secondary 
port. Each port has a range of VLAN IDs that are available for use.

We have incoming requests to reserve VLAN IDs for later use. Once a request reserves a VLAN ID on a 
particular port and device, no other request may reserve that VLAN ID on that port and device. 
There are two types of requests:
1. Requests that do not require redundancy - for these we would like to reserve a single VLAN ID 
that meets the following criteria:
    a) The VLAN ID should be the lowest available VLAN ID on any ​primary​ port.
    b) In the event of a tie, we would choose the VLAN ID on the device with the lowest device ID
2. Requests that require redundancy - for these we would like to reserve a pair of VLAN IDs that 
meet the following criteria:
    a) One VLAN ID must be from a ​primary​ port and the other must be from a secondary​ port
    b) The two ports must be on the same device
    c) The VLAN IDs must be the same on both ports
    d) The VLAN IDs should be the lowest possible IDs that meet the above criteria
    e) Again, in the event of a tie, we would choose the VLAN IDs on the device with the lowest 
    device ID

## Solution
This program takes vlans.csv and requests.csv as input and produces an output.csv file that 
specifies which requests reserved which VLAN IDs on which port and device. There is one row for a 
request that does not require redundancy and two rows for a request that does require redundancy. 
The output.csv is sorted first by ascending request_id and then by ascending primary_port.

As an optimization, the vlans data is sorted outside the provisioner and stored as a list inside 
the overall data structure before passing it to the provisioner. This way, sorting happens once 
initially instead of the provisioner having to sort during each request to find the minimum vlan 
id. Instead of doing the sorting outside of the provisioner, it could just as easily be done inside 
the provisioner upon instantiation.

A synchronous approach was taken for the solution to this problem given the relatively simple 
constraints; however, it was written in a modular fashion for easier portability to a 
multi-process, asynchronous architecture. The provisioner in the solution is essentially simulating 
utilizing a locked cache for acquiring/releasing vlan ids. The code can be adaptable to a highly 
available cloud environment.

## Running the Provisioner
```bash
python3 provisioner.py <vlans_csv> <requests_csv> <output_csv>
```

## Running Unit Tests
```bash
python3 tests.py
```

## CSV Specifications
1. **vlans.csv** - This file specifies which devices have secondary ports and which VLAN IDs are 
available on every port prior to any requests
    a) device_id - Unique integer specifying a device
    b) primary_port - 1 if the port is the primary port on the device, 0 if it is the secondary 
    port on the device
    c) vlan_id - Integer value representing the VLAN ID
2. **requests.csv** - This file specifies the requests in the order they are received and whether they 
require redundancy
    a) request_id - Unique integer specifying a request
    b) redundant - 1 if the request requires redundancy, and 0 if it does not
3. **output.csv** - This file is produced by the program and specifies which VLAN IDs on which port and 
device were reserved for which requests.
    a) request_id - Integer reference to the request_id in requests.csv
    b) device_id - Integer reference to the device_id in vlans.csv
    c) primary_port - 1 if the port is the primary port on the device, 0 if it is the secondary 
    port on the device
    d) vlan_id - Integer value representing the VLAN ID reserved


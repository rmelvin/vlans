import csv
import os
import unittest
from unittest import TestCase

from provision import IncompatibleVlansFile, read_requests, read_vlans, VlanProvisioner


class TestProvisioner(TestCase):
    """Tests for the `VlanProvisioner`"""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Read input files
        vlans_file = os.path.join(os.path.dirname(__file__), 'test_vlans.csv')
        requests_file = os.path.join(os.path.dirname(__file__), 'test_requests.csv')
        cls.vlans = read_vlans(vlans_file)
        cls.requests = read_requests(requests_file)

    def test_incompatible_vlans_csv(self):
        """
        Verify we raise a specific exception when reading an incompatible vlans csv.
        """
        csvfile = os.path.join(os.path.dirname(__file__), 'bad_vlans.csv')
        with self.assertRaises(IncompatibleVlansFile):
            read_vlans(csvfile)

    def test_min_vlan_device_pairs(self):
        """
        Verify we can find the minimum vlan id per device and return a list of (vlan_id, device_id)
        tuples sorted by vlan_id.
        """
        data = {
            0: {'primary_port': [10, 12, 13, 15], 'secondary_port': [11, 12, 13, 15]},
            1: {'primary_port': [2, 3, 5], 'secondary_port': [2, 4, 5]},
            2: {'primary_port': [0, 13, 15], 'secondary_port': [0, 1, 13]},
        }
        provisioner = VlanProvisioner(data)
        expected_sorted_pairs = [(0, 2), (2, 1), (10, 0)]  # (vlan_id, device_id)
        self.assertEqual(expected_sorted_pairs, provisioner._find_min_vlan_device_pairs())

    def test_port_match(self):
        """
        Verify we can filter a list of (vlan_id, device_id) pairs by...if the vlan_id also lives in
        the secondary port.
        """
        data = {
            0: {'primary_port': [10, 12, 13, 15], 'secondary_port': [11, 12, 13, 15]},
            1: {'primary_port': [2, 3, 5], 'secondary_port': [2, 4, 5]},
            2: {'primary_port': [0, 13, 15], 'secondary_port': [0, 1, 13]},
        }
        provisioner = VlanProvisioner(data)
        sorted_pairs = [(0, 2), (2, 1), (10, 0)]  # (vlan_id, device_id)
        expected_secondary_match_pairs = [(0, 2), (2, 1)]
        self.assertEqual(
            expected_secondary_match_pairs, provisioner._filter_by_port_match(sorted_pairs))

    def test_release_vlan(self):
        """
        Verify vlan ids are released after a request is processed.
        - one vlan id for non-redundant request
        - two vlan ids for redundant request (one for primary_port, one for secondary_port)
        """
        # Mock some vlans data.
        min_vlan_id_request0 = 0
        min_vlan_id_request1 = 2
        device1_id = 1
        device1_primary_port_vlans = [min_vlan_id_request1, 3, 5]
        device1_secondary_port_vlans = [min_vlan_id_request1, 4, 5]
        device2_id = 2
        device2_primary_port_vlans = [min_vlan_id_request0, 13, 15]
        data = {
            0: {'primary_port': [10, 12, 13, 15], 'secondary_port': [11, 12, 13, 15]},
            device1_id: {'primary_port': device1_primary_port_vlans.copy(),
                         'secondary_port': device1_secondary_port_vlans.copy()},
            device2_id: {'primary_port': device2_primary_port_vlans.copy(),
                        'secondary_port': [0, 1, 13]},
        }

        # Ensure non-redundant request releases one vlan from correct device's primary port.
        provisioner = VlanProvisioner(data)
        provisioner.process_request({'request_id': 0, 'redundant': 0})
        device2_primary_port_vlans.remove(min_vlan_id_request0)
        self.assertListEqual(
            device2_primary_port_vlans, provisioner._cache[device2_id]['primary_port'])

        # Ensure redundant request releases two vlans from correct device (one per port type).
        provisioner.process_request({'request_id': 1, 'redundant': 1})
        device1_primary_port_vlans.remove(min_vlan_id_request1)
        device1_secondary_port_vlans.remove(min_vlan_id_request1)
        self.assertListEqual(
            device1_primary_port_vlans, provisioner._cache[device1_id]['primary_port'])
        self.assertListEqual(
            device1_secondary_port_vlans, provisioner._cache[device1_id]['secondary_port'])

    def test_end_to_end(self):
        """
        Perform full end-to-end test via actual csv files to verify `VlanProvisioner` processes
        requests and outputs expected results.
        """
        actual_output_file = os.path.join(os.path.dirname(__file__), 'test_output_actual.csv')

        # Instantiate our vlan provisioner, process requests, and output results to csv.
        provisioner = VlanProvisioner(self.vlans)
        results = provisioner.process_requests(self.requests)
        provisioner.output_results(actual_output_file, results)

        # Verify actual output csv matches expected output csv.
        expected_output_file = os.path.join(os.path.dirname(__file__), 'test_output.csv')
        with open(expected_output_file) as fptr:
            reader = csv.DictReader(fptr)
            expected_rows = [row for row in reader]
        with open(actual_output_file) as fptr:
            reader = csv.DictReader(fptr)
            actual_rows = [row for row in reader]
        self.assertListEqual(expected_rows, actual_rows)

        # Remove generated file
        os.unlink(actual_output_file)


if __name__ == '__main__':
    unittest.main(verbosity=2, failfast=True)
